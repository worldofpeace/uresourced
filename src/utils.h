/* SPDX-License-Identifier: LGPL-2.1+ */

#include <glib-2.0/glib.h>
#include <sys/types.h>

int uid_cmp (gconstpointer a, gconstpointer b);
guint64 get_available_ram ();
